# SPDX-License-Identifier: GPL-3.0-or-later

from PyQt5 import QtCore, QtGui, QtWidgets

from .. import yabridgectl
from .util import _translate, resource


class Ui_statusDialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName('statusDialog')
        Dialog.resize(400, 300)
        logo = resource('data/icons/logo.png')
        icon = QtGui.QIcon()
        icon.addPixmap(
            QtGui.QPixmap(logo), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.verticalLayout = QtWidgets.QVBoxLayout(Dialog)
        self.verticalLayout.setObjectName('verticalLayout')
        self.lineEdit = QtWidgets.QLineEdit(Dialog)
        self.lineEdit.setAlignment(
            QtCore.Qt.AlignLeft | QtCore.Qt.AlignLeading |
            QtCore.Qt.AlignVCenter)
        self.lineEdit.setObjectName('lineEdit')
        self.lineEdit.textChanged.connect(lambda x: self.find(x))
        self.verticalLayout.addWidget(self.lineEdit)
        self.tableWidget = QtWidgets.QTableWidget(Dialog)
        self.tableWidget.setDragDropOverwriteMode(False)
        self.tableWidget.setAlternatingRowColors(True)
        self.tableWidget.setObjectName('tableWidget')
        self.tableWidget.setColumnCount(6)
        self.tableWidget.setRowCount(0)
        item = QtWidgets.QTableWidgetItem()
        item.setTextAlignment(
            QtCore.Qt.AlignLeading | QtCore.Qt.AlignVCenter)
        self.tableWidget.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        item.setTextAlignment(QtCore.Qt.AlignCenter)
        self.tableWidget.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        item.setTextAlignment(QtCore.Qt.AlignCenter)
        self.tableWidget.setHorizontalHeaderItem(2, item)
        item = QtWidgets.QTableWidgetItem()
        item.setTextAlignment(QtCore.Qt.AlignCenter)
        self.tableWidget.setHorizontalHeaderItem(3, item)
        item = QtWidgets.QTableWidgetItem()
        item.setTextAlignment(QtCore.Qt.AlignCenter)
        self.tableWidget.setHorizontalHeaderItem(4, item)
        item = QtWidgets.QTableWidgetItem()
        item.setTextAlignment(
            QtCore.Qt.AlignLeading | QtCore.Qt.AlignVCenter)
        self.tableWidget.setHorizontalHeaderItem(5, item)
        self.tableWidget.horizontalHeader().setStretchLastSection(True)
        self.tableWidget.horizontalHeader().setSectionResizeMode(
            0, QtWidgets.QHeaderView.ResizeToContents)
        self.verticalLayout.addWidget(self.tableWidget)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName('horizontalLayout')
        self.label_1 = QtWidgets.QLabel(Dialog)
        self.label_1.setText('')
        self.label_1.setObjectName('label_1')
        self.horizontalLayout.addWidget(self.label_1)
        self.line = QtWidgets.QFrame(Dialog)
        self.line.setFrameShape(QtWidgets.QFrame.VLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName('line')
        self.horizontalLayout.addWidget(self.line)
        self.label_2 = QtWidgets.QLabel(Dialog)
        self.label_2.setText('')
        self.label_2.setObjectName('label_2')
        self.horizontalLayout.addWidget(self.label_2)
        self.line_2 = QtWidgets.QFrame(Dialog)
        self.line_2.setFrameShape(QtWidgets.QFrame.VLine)
        self.line_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_2.setObjectName('line_2')
        self.horizontalLayout.addWidget(self.line_2)
        self.label_3 = QtWidgets.QLabel(Dialog)
        self.label_3.setText('')
        self.label_3.setObjectName('label_3')
        self.horizontalLayout.addWidget(self.label_3)
        self.line_3 = QtWidgets.QFrame(Dialog)
        self.line_3.setFrameShape(QtWidgets.QFrame.VLine)
        self.line_3.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_3.setObjectName('line_3')
        self.horizontalLayout.addWidget(self.line_3)
        self.label_4 = QtWidgets.QLabel(Dialog)
        self.label_4.setText('')
        self.label_4.setObjectName('label_4')
        self.horizontalLayout.addWidget(self.label_4)
        self.verticalLayout.addLayout(self.horizontalLayout)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)
        self.status = yabridgectl.main.status()
        self.show_table(self.status)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(_translate('Dialog', 'yabridget - status'))
        self.lineEdit.setPlaceholderText(
            _translate('statusDialog', 'Filter by name...'))
        self.tableWidget.setSortingEnabled(True)
        item = self.tableWidget.horizontalHeaderItem(0)
        item.setText(_translate('statusDialog', 'Name'))
        item = self.tableWidget.horizontalHeaderItem(1)
        item.setText(_translate('statusDialog', 'Type'))
        item = self.tableWidget.horizontalHeaderItem(2)
        item.setText(_translate('statusDialog', 'Legacy?'))
        item = self.tableWidget.horizontalHeaderItem(3)
        item.setText(_translate('statusDialog', 'Arch'))
        item = self.tableWidget.horizontalHeaderItem(4)
        item.setText(_translate('statusDialog', 'Method'))
        item = self.tableWidget.horizontalHeaderItem(5)
        item.setText(_translate('statusDialog', 'Path'))

    def show_table(self, status_):
        self.label_1.setText(status_['path'])
        self.label_2.setText(status_['vst'])
        self.label_3.setText(status_['vstt'])
        self.label_4.setText(status_['method'])
        if status_.get('rest'):
            self.tableWidget.setRowCount(len(status_['result']))
            for i, j in enumerate(status_['result']):
                for k, l in enumerate(j):
                    item = QtWidgets.QTableWidgetItem(l)
                    item.setFlags(item.flags() & ~QtCore.Qt.ItemIsEditable)
                    if k != 0 and k != len(j) - 1:
                        item.setTextAlignment(QtCore.Qt.AlignCenter)
                    self.tableWidget.setItem(i, k, item)

    def find(self, text):
        if not self.status.get('result'):
            return
        if text:
            status = self.status.copy()
            status['result'] = tuple(
                i for i in self.status['result']
                if text.lower() in i[0].lower())
            self.show_table(status)
        else:
            self.show_table(self.status)
